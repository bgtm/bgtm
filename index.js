var plumber = require('gulp-plumber');
var deepExtend = require('deep-extend');
var livereload = require('gulp-livereload');
var cached = require('gulp-cached');
var remember = require('gulp-remember');
var through2 = require('through2');

module.exports = function (gulp) {
    /**
     * There are some flags available that set the mode for this runner and various options
     * for subsequent build packages.
     *
     * --production - If you set this value it will swap the node environment over to production mode
     * To specify a custom environment you can use the following:
     *
     * --environment <ENVIRONMENT NAME>
     *
     * The runner mode is sent through to the engines as well and can be used as part of the engines.
     *
     * @constructor
     */
    function TaskRunner() {
        this.tasks = [];
        this.onBuildTasks = [];
        this.watchers = [];
        this.environment = {
            ALL: 'all',
            DEVELOPMENT: 'development',
            PRODUCTION: 'production'
        };
        this.currentEnvironment = this.environment.DEVELOPMENT;
        if (process.argv.indexOf('--production') > -1) {
            this.currentEnvironment = this.environment.PRODUCTION;
        }

        // Setting custom environment
        var environmentParameter = process.argv.indexOf('--environment');
        if (environmentParameter > -1) {
            if (typeof process.argv[environmentParameter + 1] !== 'undefined') {
                this.currentEnvironment = process.argv[environmentParameter + 1];
            }
        }

        if (this.isEnvironment(this.environment.PRODUCTION)) {
            process.env.NODE_ENV = 'production';
        }

        this.runningTasks = [];
        this.currentRunningTask = '';
    }

    /**
     * Checks if the current running environment is the same as you are expecting.
     *
     * @param env
     * @returns {boolean}
     */
    TaskRunner.prototype.isEnvironment = function isEnvironment(env) {
        return (this.currentEnvironment === env);
    };

    /**
     * Callback method for the watcher task that is used to run the task
     * watching.
     *
     * It also displays messages about which files have been updated.
     *
     * @param watchers
     * @constructor
     */
    TaskRunner.prototype.taskWatcher = function taskWatcher() {
        livereload.listen();
        var self = this;
        for (var key in this.watchers) {
            if (this.watchers.hasOwnProperty(key)) {
                var watcher = this.watchers[key];
                gulp.watch(watcher.glob, gulp.series(watcher.taskName));
            }
        }
    };

    TaskRunner.prototype.IncrementalGet = function IncrementalGet() {
        return cached(this.currentRunningTask);
    };
    TaskRunner.prototype.IncrementalSet = function IncrementalSet() {
        return remember(this.currentRunningTask);
    };
    TaskRunner.prototype.IncrementalRemoveFile = function IncrementalRemoveFile(filePath) {
        if (cached.caches[this.currentRunningTask]) {
            delete cached.caches[this.currentRunningTask][filePath];       // gulp-cached remove api
        }
        remember.forget(this.currentRunningTask, filePath);         // gulp-remember remove api
    };

    /**
     * Adds a new task to the Task Runner so that it's ready to be run.
     *
     * @param taskName
     * @param args
     * @constructor
     */
    TaskRunner.prototype.add = function add(taskName, args) {
        var defaults = {
            /**
             * Whether to run this on build.
             */
            runOnBuild: false,
            /**
             * Specify the environment you want this task to be run on for the build environment.
             *
             * This also supports an array of environments to run on.
             */
            runOnBuildEnvironment: this.environment.ALL,
            /**
             * Set up a file watcher for this?
             */
            watch: false,
            /**
             * Which files to watch?
             */
            watchSource: [],
            /**
             * Enable Live Reload for this task?
             */
            liveReload: false,
            /**
             * The engine to run, this can be a closure or it can be a predefined extension engine.
             */
            engine: function () {
            },
            /**
             * Where the engine options are set to be passed to the task engine?
             */
            engineOptions: {}
        };

        var config = deepExtend({}, defaults, args);
        config.name = taskName;

        this.tasks.push({
            type: 'task',
            name: config.name,
            data: config
        });
    };

    TaskRunner.prototype.addSequence = function addSequence(seriesName, args, callback) {
        console.log('[DEPRECATED] The addSequence method is no longer valid, it will still run but will be removed in future versions, please use addSeries in your \'gulpfile.js\' instead.');
        this.addSeries(seriesName, args, callback);
    };

        /**
     * Allows for you sequence the tasks added inside and run them in order off them being added.
     *
     * Note: that all the options except for engine and engineOptions are ignored, so these tasks
     * will not be run on build by default.
     *
     * However the sequence task will be run on build if specified to.
     *
     * @param taskName
     * @param args
     * @param callback A function with 2 parameters where the first is the task manager.
     */
    TaskRunner.prototype.addSeries = function addSeries(seriesName, args, callback) {
        var defaultArgs = {
            runOnBuild: false,
            runOnBuildEnvironment: this.environment.ALL,
            watch: false,
            liveReload: false
        }
        args = deepExtend({}, defaultArgs, args);

        var tr = this;
        if(typeof callback === 'function') {
            var sequenceTasks = [];
            var sequenceManager = {
                add: function(taskName, args) {
                    args.runOnBuild = false;
                    args.liveReload = false;
                    args.name = seriesName+'-'+taskName;
                    sequenceTasks.push({
                        type: 'task',
                        data: args
                    });
                }
            };

            callback.call(null, sequenceManager);

            this.tasks.push({
                type: 'series',
                name: seriesName,
                data: {
                    args: args,
                    tasks: sequenceTasks
                }
            })
        }
    };

    // For non gulp tasks we want to check the task has ended.
    TaskRunner.prototype.onTaskEnd = function onTaskEnd(taskName, liveReload, callback, sanity) {
        sanity = sanity || 500; // default sanity of 50 seconds
        var self = this;
        if(this.runningTasks.indexOf(taskName) < 0) {
            callback();
            if(liveReload) {
                livereload.reload();
            }
        }
        else {
            setTimeout(function() {
                self.onTaskEnd(taskName, liveReload, callback);
            }, 100);
        }
    }

    TaskRunner.prototype.addGulpTasks = function processGulpTasks(taskItem) {
        var self = this;
        if(taskItem.type === 'task') {
            // Add the gulp tasks.
            gulp.task(
                taskItem.data.name,
                (function (taskManager, g, item) {
                    taskManager.runningTasks.push(item.name);
                    return function (callback) {
                        taskManager.currentRunningTask = item.name;
                        if (typeof item.engine === 'function') {
                            var results = item.engine.apply(g, [taskManager, item.engineOptions, function() {
                                var through = through2.obj();
                                if (through.pipe && item.liveReload) {
                                    through.pipe(livereload())
                                }
                                // Remove from the current running list.
                                taskManager.runningTasks.splice(taskManager.runningTasks.indexOf(item.name), 1);
                                return through;
                            }]);
                            if(typeof results != 'undefined' && typeof results.pipe !== 'undefined' && typeof results.pipe2 !== 'undefined') {
                                return results;
                            }
                            else {
                                self.onTaskEnd(item.name, item.liveReload, callback);
                            }
                            taskManager.currentRunningTask = '';
                        }
                        else {
                            taskManager.currentRunningTask = '';
                            throw new Error("The option for engine must be a function or closure.");
                        }
                    }
                })(this, gulp, taskItem.data)
            );

            if(taskItem.data.runOnBuild) {
                this.addRunOnBuild(taskItem.data.name, taskItem.data.runOnBuildEnvironment);
            }

            if (taskItem.data.watch) {
                this.watchers.push({
                    glob: taskItem.data.watchSource,
                    taskName: taskItem.data.name
                });
            }
        }
        else if(taskItem.type === 'series') {
            for(var i = 0; i < taskItem.data.tasks.length; i++) {
                var clonedTask = deepExtend({}, taskItem.data.tasks[i]);
                clonedTask.data.watch = false;
                this.addGulpTasks(clonedTask);
            }

            if(taskItem.data.args.watch) {
                var watchTasks = taskItem.data.tasks.filter(function(i) {
                    return i.data.watch;
                }).map(function(i) {
                    return {
                        glob: i.data.watchSource,
                        taskName: i.data.name
                    };
                });
                if(watchTasks.length > 0) {
                    var globs = [];
                    for(var i = 0; i < watchTasks.length; i++) {
                        globs = globs.concat(watchTasks[i].glob);
                    }
                    this.watchers.push({
                        glob: globs,
                        taskName: taskItem.name
                    });
                }
            }

            var seriesTasks = taskItem.data.tasks.map(function(i) {
                return i.data.name;
            });

            if(seriesTasks.length > 0) {
                gulp.task(taskItem.name, gulp.series.apply(null, seriesTasks))
            }

            if(taskItem.data.args.runOnBuild) {
                this.addRunOnBuild(taskItem.name, taskItem.data.args.runOnBuildEnvironment);
            }
        }
    };

    TaskRunner.prototype.addRunOnBuild = function addRunOnBuild(taskName, runOnBuildEnvironment) {
        var self = this;
        if(!(runOnBuildEnvironment instanceof Array)) {
            runOnBuildEnvironment = [runOnBuildEnvironment];
        }

        var allowedEnvironments = runOnBuildEnvironment.filter(function (item) {
            if (item === self.environment.ALL) {
                return true;
            }
            else if (self.isEnvironment(item)) {
                return true;
            }
            return false;
        });

        if (allowedEnvironments.length > 0) {
            this.onBuildTasks.push(taskName);
        }
    }

    /**
     * Sets the tasks up in the system ready for watching and running and
     * also initialises all the gulp running. This should always be run after
     * all the tasks have been added.
     */
    TaskRunner.prototype.run = function run() {
        for (var i = 0; i < this.tasks.length; i++) {
            var item = this.tasks[i];
            this.addGulpTasks(item);
        }
        gulp.task('build', gulp.parallel.apply(null, this.onBuildTasks));
        gulp.task('compile', gulp.parallel.apply(null, this.onBuildTasks)); // alias for build
        gulp.task('watch', this.taskWatcher.bind(this));
        gulp.task('default', gulp.series('build', 'watch'));
    };

    return new TaskRunner();
};